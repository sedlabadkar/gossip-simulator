README

Description:
* Akka Actor simulator for Gossip Protocol and Push Sum Algorithms

* Simulates 4 topologies: Full Network, Line, 3D Grid and 3D imperfect Grid.

* Full Network: Every simulated node is connected to every other node

* Line Network: All nodes are located along a line. Each node is connected to two neighbors. One on its left and other on its right. 

* 3D Grid: Nodes form a 3D grid

* 3D Imperfect Grid: Nodes for a 3d Grid. Each node has one random neighbor node in addition to the regular neighbors of the 3D grid.

Execution:

* Requirements : sbt, scala

* sbt should take care of all dependencies. build.sbt is included.

* sbt "run <numNodes> <Topology> <algorithm>"

1. numNodes : Number of nodes to simulate. For 3D topologies, this represents the number of nodes on the edge. This number will be cubed to create the topology.

2. Topology : Topology to simulate. Eg. full, line, 3d, imprefect3d

3. Algorithm : Gossip or Push-sum

**NOTE:** With large number of nodes in inefficient topologies such as Line network, it is possible that some nodes may terminate and kill the message exchange while others are still busy transmitting. 